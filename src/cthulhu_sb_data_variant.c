/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>

#include <amxc/amxc.h>

#include <cthulhu/cthulhu_sb_data_variant.h>
#include <cthulhu_priv.h>
#include <cthulhu_assert.h>

static int variant_cthulhu_sb_data_init(amxc_var_t* const var) {
    var->data.data = NULL;

    return 0;
}

static void variant_cthulhu_sb_data_delete(amxc_var_t* var) {
    var->data.data = NULL;
}

static int variant_cthulhu_sb_data_copy(amxc_var_t* const dest,
                                        const amxc_var_t* const src) {
    cthulhu_sb_data_t* src_data = (cthulhu_sb_data_t*) src->data.data;

    dest->data.data = src_data;
    return 0;
}

static int variant_cthulhu_sb_data_move(amxc_var_t* const dest,
                                        amxc_var_t* const src) {
    dest->data.data = src->data.data;
    src->data.data = NULL;
    return 0;
}


static int variant_cthulhu_sb_data_convert_to_null(amxc_var_t* const dest,
                                                   CTHULHU_UNUSED const amxc_var_t* const src) {
    dest->data.data = NULL;
    return 0;
}

CTHULHU_INLINE int string_list_to_string(amxc_string_t* string,
                                         const amxc_llist_t* llist,
                                         const char* name) {
    int retval = -1;
    if(!amxc_llist_is_empty(llist)) {
        amxc_string_appendf(string, "%s:\n", name);
        amxc_llist_for_each(it, llist) {
            amxc_var_t* var = amxc_var_from_llist_it(it);
            const char* txt = amxc_var_constcast(cstring_t, var);
            when_failed(amxc_string_appendf(string, "    %s\n", txt), exit);
        }
    }
    retval = 0;
exit:
    return retval;
}

CTHULHU_INLINE int string_htable_to_string(amxc_string_t* string,
                                           const amxc_htable_t* htable,
                                           const char* name) {
    int retval = -1;
    if(!amxc_htable_is_empty(htable)) {
        amxc_string_appendf(string, "%s:\n", name);
        amxc_htable_for_each(it, htable) {
            amxc_var_t* var = amxc_var_from_llist_it(it);
            const char* key = amxc_htable_it_get_key(it);
            const char* txt = amxc_var_constcast(cstring_t, var);
            when_failed(amxc_string_appendf(string, "    %s: %s\n", key, txt), exit);
        }
    }
    retval = 0;
exit:
    return retval;
}

static int variant_cthulhu_sb_data_convert_to_string(amxc_var_t* const dest,
                                                     const amxc_var_t* const src) {
    int retval = -1;
    const cthulhu_sb_data_t* sb_data = (cthulhu_sb_data_t*) src->data.data;
    amxc_string_t string;
    when_failed(amxc_string_init(&string, 0), exit);
    when_failed(amxc_string_appendf(&string, "Id: %s\n", sb_data->sb_id), exit);
    when_failed(amxc_string_appendf(&string, "Namespace PID: %d\n", sb_data->ns_pid), exit);
    when_failed(amxc_string_appendf(&string, "Namespace UTS: %d\n", sb_data->ns_inherrit_uts), exit);
    when_failed(amxc_string_appendf(&string, "Namespace Net: %d\n", sb_data->ns_inherrit_net), exit);
    when_failed(amxc_string_appendf(&string, "Namespace User: %d\n", sb_data->ns_inherrit_user), exit);
    when_failed(amxc_string_appendf(&string, "Namespace Mount: %d\n", sb_data->ns_inherrit_mnt), exit);

    free(dest->data.s);
    dest->data.s = amxc_string_take_buffer(&string);
    retval = 0;

exit:
    amxc_string_clean(&string);
    return retval;
}

static int variant_cthulhu_sb_data_convert_to(amxc_var_t* const dest,
                                              const amxc_var_t* const src) {
    int retval = -1;

    amxc_var_convert_fn_t convfn[AMXC_VAR_ID_CUSTOM_BASE] = {
        variant_cthulhu_sb_data_convert_to_null,        // NULL
        variant_cthulhu_sb_data_convert_to_string,      // CSTRING
        NULL,                                           // INT8
        NULL,                                           // INT16
        NULL,                                           // INT32
        NULL,                                           // INT64
        NULL,                                           // UINT8
        NULL,                                           // UIN16
        NULL,                                           // UINT32
        NULL,                                           // UINT64
        NULL,                                           // FLOAT
        NULL,                                           // DOUBLE
        NULL,                                           // BOOL
        NULL,                                           // LIST
        NULL,                                           // HTABLE
        NULL,                                           // FD
        NULL,                                           // TIMESTAMP
        NULL,                                           // CSV_STRING
        NULL,                                           // SSV_STRING
        variant_cthulhu_sb_data_convert_to_string       // ANY
    };

    if(dest->type_id >= AMXC_VAR_ID_CUSTOM_BASE) {
        goto exit;
    }

    if(convfn[dest->type_id] != NULL) {
        if(dest->type_id == AMXC_VAR_ID_ANY) {
            amxc_var_set_type(dest, AMXC_VAR_ID_CSTRING);
        }
        retval = convfn[dest->type_id](dest, src);
    }

exit:
    return retval;
}

static amxc_var_type_t variant_cthulhu_sb_data = {
    .init = variant_cthulhu_sb_data_init,
    .del = variant_cthulhu_sb_data_delete,
    .copy = variant_cthulhu_sb_data_copy,
    .move = variant_cthulhu_sb_data_move,
    .convert_from = NULL,
    .convert_to = variant_cthulhu_sb_data_convert_to,
    .compare = NULL,
    .get_key = NULL,
    .set_key = NULL,
    .get_index = NULL,
    .set_index = NULL,
    .type_id = 0,
    .hit = { .ait = NULL, .key = NULL, .next = NULL },
    .name = CTHULHU_VAR_NAME_SB_DATA
};

CTHULHU_CONSTRUCTOR static void variant_cthulhu_sb_data_register(void) {
    amxc_var_register_type(&variant_cthulhu_sb_data);
}

CTHULHU_DESTRUCTOR static void variant_cthulhu_sb_data_unregister(void) {
    amxc_var_unregister_type(&variant_cthulhu_sb_data);
}

const cthulhu_sb_data_t* amxc_var_get_const_cthulhu_sb_data_t(const amxc_var_t* const var) {
    cthulhu_sb_data_t* retval = NULL;

    when_null(var, exit);
    when_true(var->type_id != CTHULHU_VAR_ID_SB_DATA, exit);

    retval = (cthulhu_sb_data_t*) var->data.data;
exit:
    return retval;
}

cthulhu_sb_data_t* amxc_var_take_cthulhu_sb_data_t(amxc_var_t* const var) {
    cthulhu_sb_data_t* retval = NULL;

    when_null(var, exit);
    when_true(var->type_id != CTHULHU_VAR_ID_SB_DATA, exit);

    retval = (cthulhu_sb_data_t*) var->data.data;
    var->data.data = NULL;
    var->type_id = AMXC_VAR_ID_NULL;

exit:
    return retval;
}

int amxc_var_set_cthulhu_sb_data_t(amxc_var_t* const var, cthulhu_sb_data_t* const val) {
    int retval = -1;

    when_null(var, exit);
    when_null(val, exit);
    when_failed(amxc_var_set_type(var, CTHULHU_VAR_ID_SB_DATA), exit);

    var->data.data = val;

    retval = 0;

exit:
    return retval;
}

amxc_var_t* amxc_var_add_new_cthulhu_sb_data_t(amxc_var_t* const var,
                                               cthulhu_sb_data_t* val) {
    amxc_var_t* subvar = NULL;

    when_null(var, exit);
    subvar = amxc_var_add_new(var);
    when_null(subvar, exit);

    if(amxc_var_set_cthulhu_sb_data_t(subvar, val) != 0) {
        amxc_var_delete(&subvar);
    }

exit:
    return subvar;
}

amxc_var_t* amxc_var_add_new_key_cthulhu_sb_data_t(amxc_var_t* const var,
                                                   const char* key,
                                                   cthulhu_sb_data_t* val) {
    amxc_var_t* subvar = NULL;

    when_null(var, exit);
    subvar = amxc_var_add_new_key(var, key);
    when_null(subvar, exit);

    if(amxc_var_set_cthulhu_sb_data_t(subvar, val) != 0) {
        amxc_var_delete(&subvar);
    }

exit:
    return subvar;
}



