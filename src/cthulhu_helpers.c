/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define _GNU_SOURCE 1 // to make strdup work

#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <alloca.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include <sys/wait.h>
#include <debug/sahtrace.h>

#include <cthulhu/cthulhu_helpers.h>


#define DEV_RANDOM "/dev/urandom"
#define string_is_empty(s) (!s || !*s)
#define when_null(x, l) if(x == NULL) { goto l; }

#define ME "helpers"

bool cthulhu_isdir(const char* path) {
    struct stat sb;

    if(stat(path, &sb) < 0) {
        return false;
    }

    return S_ISDIR(sb.st_mode);
}

bool cthulhu_isfile(const char* path) {
    struct stat sb;

    if(stat(path, &sb) < 0) {
        return false;
    }

    return S_ISREG(sb.st_mode);
}


bool cthulhu_issocket(const char* path) {
    struct stat sb;

    if(stat(path, &sb) < 0) {
        return false;
    }

    return S_ISSOCK(sb.st_mode);
}

/**
 * @brief helper to remove a dir
 *
 * @param dir
 * @return int 0 on success, -1 on failure
 */
int cthulhu_rmdir(const char* dir) {
    int res = -1;
    char* cmd = NULL;
    if(string_is_empty(dir)) {
        return res;
    }
    if(asprintf(&cmd, "rm -rf %s", dir) < 0) {
        printf("Could not allocated mem for string");
        return res;
    }
    res = system(cmd);
    free(cmd);
    return res;
}

/**
 * @brief helper to make a dir
 *
 * @param dir
 * @param recurcive make intermediate dirs also
 * @return int 0 on success, -1 on failure
 */
int cthulhu_mkdir(const char* dir, bool recursive) {
    return cthulhu_mkdir_ext(dir, 0, 0, 0755, recursive);
}

/**
 * @brief helper to make a dir
 *
 * @param dir
 * @param uid change ownership if != 0
 * @param gid change ownership if != 0
 * @param mode mode of dir
 * @param recurcive make intermediate dirs also
 *
 * @return int 0 on success, -1 on failure
 */
int cthulhu_mkdir_ext(const char* dir, uid_t uid, gid_t gid, mode_t mode, bool recursive) {
    int res = -1;
    char* dir_cpy = NULL;
    char* p = NULL;
    if(string_is_empty(dir)) {
        return res;
    }

    if(recursive) {
        dir_cpy = strdup(dir);

        p = dir_cpy;
        while(*p && *p == '/') {
            p++;
        }

        while(*p) {
            while(*p && *p != '/') {
                p++;
            }
            if(*p != '/') {
                break;
            }
            *p = '\0';
            if(mkdir(dir_cpy, mode) != 0) {
                if(errno != EEXIST) {
                    goto exit;
                }
            } else if(uid || gid) {
                if(chown(dir, uid, gid) != 0) {
                    goto exit;
                }
            }
            *p = '/';
            while(*p && *p == '/') {
                p++;
            }
        }
    }
    if(mkdir(dir, mode) != 0) {
        if(errno != EEXIST) {
            goto exit;
        }
    } else if(uid || gid) {
        if(chown(dir, uid, gid) != 0) {
            goto exit;
        }
    }
    res = 0;
exit:
    free(dir_cpy);
    return res;
}

int cthulhu_wait_for_pid(pid_t pid) {
    int res = -1;
    int status = 0;

    do {
        res = waitpid(pid, &status, 0);
    } while (res < 0 && errno == EINTR);

    if(!WIFEXITED(status) || (WEXITSTATUS(status) != 0)) {
        return -1;
    }

    return 0;
}

/**
 * @brief Execute a system command
 *
 * @param cmd
 * @return int return value of the command
 */
int cthulhu_proc_wait(const char* cmd, int* exit_status) {
    int res = -1;
    pid_t child_pid;
    when_null(cmd, exit);

    child_pid = fork();
    if(child_pid < 0) {
        goto exit;
    }

    if(!child_pid) {
        sigset_t mask;
        /* unblock all signals */
        res = sigfillset(&mask);
        if(res < 0) {
            goto exit;
        }
        res = pthread_sigmask(SIG_UNBLOCK, &mask, NULL);
        if(res < 0) {
            goto exit;
        }

        execl("/bin/sh", "sh", "-c", cmd, (char*) NULL);
        printf("Could not exec %s", cmd);
        _exit(EXIT_FAILURE);
    } else {
        int status;
        int rc;
        do {
            rc = waitpid(child_pid, &status, 0);
        } while (rc < 0 && errno == EINTR);
        if(WIFEXITED(status)) {
            if(exit_status) {
                *exit_status = WEXITSTATUS(status);
            }
            res = 0;
        }
    }

exit:
    return res;
}

/**
 * @brief Fill single element of n_bytes with /dev/urandom
 *  usually meant for small random's like integers
 *
 * @param random_output pointer to memory to output random
 * @param n_bytes amount of bytes to fill
 * @return int 0 on success, -1 on failure
 */
size_t cthulhu_get_random(void* random_output, size_t n_bytes) {
    size_t res = -1;
    FILE* urandom = fopen(DEV_RANDOM, "r");
    if(urandom == NULL) {
        return res;
    }

    if(fread(random_output, n_bytes, 1, urandom)) {
        res = n_bytes;
    }

    fclose(urandom);
    return res;
}

