/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#define _GNU_SOURCE 1 // to make strdup work

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <amxc/amxc_string.h>
#include <amxc/amxc_llist.h>
#include <amxc/amxc_htable.h>
#include <amxc/amxc_variant.h>
#include <debug/sahtrace.h>

#include <cthulhu/cthulhu_ctr_start_data.h>
#include <lcm/lcm_assert.h>

#include "cthulhu_priv.h"

#define UNUSED __attribute__((unused))

#define ME "lib"
static void cthulhu_envvar_data_it_free(amxc_llist_it_t* it);

/**************/
/* Mount data */
/**************/

int cthulhu_mount_data_new(cthulhu_mount_data_t** mount_data) {
    int retval = -1;
    when_null(mount_data, exit);

    *mount_data = (cthulhu_mount_data_t*) calloc(1, sizeof(cthulhu_mount_data_t));
    when_null(*mount_data, exit);

    if(cthulhu_mount_data_init(*mount_data) != 0) {
        cthulhu_mount_data_delete(mount_data);
        goto exit;
    }
    retval = 0;
exit:
    return retval;
}

void cthulhu_mount_data_delete(cthulhu_mount_data_t** mount_data) {
    when_null(mount_data, exit);

    cthulhu_mount_data_clean(*mount_data);

    free_null(*mount_data);

exit:
    return;
}
int cthulhu_mount_data_init(cthulhu_mount_data_t* const mount_data) {
    int retval = -1;
    when_null(mount_data, exit);
    when_failed(amxc_llist_it_init(&mount_data->lit), exit);
    mount_data->source = NULL;
    mount_data->destination = NULL;
    mount_data->type = NULL;
    mount_data->options = NULL;

    retval = 0;
exit:
    return retval;
}

void cthulhu_mount_data_clean(cthulhu_mount_data_t* const mount_data) {
    when_null(mount_data, exit);

    free_null(mount_data->source);
    free_null(mount_data->destination);
    free_null(mount_data->type);
    free_null(mount_data->options);

exit:
    return;
}

void cthulhu_mount_data_reset(cthulhu_mount_data_t* const mount_data) {
    cthulhu_mount_data_clean(mount_data);
}

static void cthulhu_mount_data_it_free(amxc_llist_it_t* it) {
    cthulhu_mount_data_t* mount_data = amxc_llist_it_get_data(it, cthulhu_mount_data_t, lit);
    cthulhu_mount_data_delete(&mount_data);
}

/***************/
/* Device data */
/***************/

int cthulhu_device_data_new(cthulhu_device_data_t** device_data) {
    int retval = -1;
    when_null(device_data, exit);

    *device_data = (cthulhu_device_data_t*) calloc(1, sizeof(cthulhu_device_data_t));
    when_null(*device_data, exit);

    if(cthulhu_device_data_init(*device_data) != 0) {
        cthulhu_device_data_delete(device_data);
        goto exit;
    }
    retval = 0;
exit:
    return retval;
}

void cthulhu_device_data_delete(cthulhu_device_data_t** device_data) {
    when_null(device_data, exit);

    cthulhu_device_data_clean(*device_data);

    free_null(*device_data);

exit:
    return;
}
int cthulhu_device_data_init(cthulhu_device_data_t* const device_data) {
    int retval = -1;
    when_null(device_data, exit);
    when_failed(amxc_llist_it_init(&device_data->lit), exit);
    device_data->device = NULL;
    device_data->major = 0;
    device_data->minor = 0;
    device_data->type = 'c';
    device_data->create = false;

    retval = 0;
exit:
    return retval;
}

void cthulhu_device_data_clean(cthulhu_device_data_t* const device_data) {
    when_null(device_data, exit);

    free_null(device_data->device);

exit:
    return;
}

void cthulhu_device_data_reset(cthulhu_device_data_t* const device_data) {
    cthulhu_device_data_clean(device_data);
}

static void cthulhu_device_data_it_free(amxc_llist_it_t* it) {
    cthulhu_device_data_t* device_data = amxc_llist_it_get_data(it, cthulhu_device_data_t, lit);
    cthulhu_device_data_delete(&device_data);
}

/********************/
/* usermapping data */
/********************/

int cthulhu_usermapping_data_new(cthulhu_usermapping_data_t** usermapping_data) {
    int retval = -1;
    when_null(usermapping_data, exit);

    *usermapping_data = (cthulhu_usermapping_data_t*) calloc(1, sizeof(cthulhu_usermapping_data_t));
    when_null(*usermapping_data, exit);

    if(cthulhu_usermapping_data_init(*usermapping_data) != 0) {
        cthulhu_usermapping_data_delete(usermapping_data);
        goto exit;
    }
    retval = 0;
exit:
    return retval;
}

void cthulhu_usermapping_data_delete(cthulhu_usermapping_data_t** usermapping_data) {
    when_null(usermapping_data, exit);

    cthulhu_usermapping_data_clean(*usermapping_data);

    free_null(*usermapping_data);

exit:
    return;
}
int cthulhu_usermapping_data_init(cthulhu_usermapping_data_t* const usermapping_data) {
    int retval = -1;
    when_null(usermapping_data, exit);
    when_failed(amxc_llist_it_init(&usermapping_data->lit), exit);
    memset(usermapping_data, 0, sizeof(cthulhu_usermapping_data_t));
    retval = 0;
exit:
    return retval;
}

void cthulhu_usermapping_data_clean(cthulhu_usermapping_data_t* const usermapping_data) {
    when_null(usermapping_data, exit);
    memset(usermapping_data, 0, sizeof(cthulhu_usermapping_data_t));
exit:
    return;
}

void cthulhu_usermapping_data_reset(cthulhu_usermapping_data_t* const usermapping_data) {
    cthulhu_usermapping_data_clean(usermapping_data);
}

void cthulhu_usermapping_data_it_free(amxc_llist_it_t* it) {
    cthulhu_usermapping_data_t* usermapping_data = amxc_llist_it_get_data(it, cthulhu_usermapping_data_t, lit);
    cthulhu_usermapping_data_delete(&usermapping_data);
}

/*******************/
/** ctr_start_data */
/*******************/

int cthulhu_ctr_start_data_new(cthulhu_ctr_start_data_t** start_data) {
    int retval = -1;
    when_null(start_data, exit);

    *start_data = (cthulhu_ctr_start_data_t*) calloc(1, sizeof(cthulhu_ctr_start_data_t));
    when_null(*start_data, exit);

    if(cthulhu_ctr_start_data_init(*start_data) != 0) {
        cthulhu_ctr_start_data_delete(start_data);
        goto exit;
    }
    retval = 0;
exit:
    return retval;
}
void cthulhu_ctr_start_data_delete(cthulhu_ctr_start_data_t** start_data) {
    when_null(start_data, exit);

    cthulhu_ctr_start_data_clean(*start_data);

    free_null(*start_data);

exit:
    return;
}

int cthulhu_ctr_start_data_init(cthulhu_ctr_start_data_t* const start_data) {
    int retval = -1;

    when_null(start_data, exit);
    start_data->sb_data = NULL;
    start_data->syslogng = NULL;
    start_data->host_data_dir = NULL;
    start_data->capabilities = NULL;
    when_failed(amxc_llist_init(&start_data->mounts), exit);
    when_failed(amxc_llist_init(&start_data->devices), exit);
    when_failed(amxc_llist_init(&start_data->envvar), exit);
    when_failed(amxc_llist_init(&start_data->usermappings), exit);

    retval = 0;
exit:
    return retval;
}
void cthulhu_ctr_start_data_clean(cthulhu_ctr_start_data_t* const start_data) {
    when_null(start_data, exit);
    free_null(start_data->syslogng);
    free_null(start_data->host_data_dir);
    free_null(start_data->capabilities);
    start_data->run_user_id = 0;
    start_data->run_group_id = 0;
    cthulhu_sb_data_delete(&start_data->sb_data);
    amxc_llist_clean(&start_data->mounts, cthulhu_mount_data_it_free);
    amxc_llist_clean(&start_data->devices, cthulhu_device_data_it_free);
    amxc_llist_clean(&start_data->envvar, cthulhu_envvar_data_it_free);
    amxc_llist_clean(&start_data->usermappings, cthulhu_usermapping_data_it_free);

exit:
    return;
}
void cthulhu_ctr_start_data_reset(cthulhu_ctr_start_data_t* const start_data) {
    cthulhu_ctr_start_data_clean(start_data);
}

int cthulhu_ctr_start_data_add_mount_data(cthulhu_ctr_start_data_t* const start_data,
                                          const char* const source,
                                          const char* const destination,
                                          const char* const type,
                                          const char* const options) {
    int retval = -1;
    cthulhu_mount_data_t* mount_data = NULL;

    when_null(start_data, exit);
    when_null(source, exit);
    when_null(destination, exit);

    when_failed(cthulhu_mount_data_new(&mount_data), exit);
    mount_data->source = strdup(source);
    mount_data->destination = strdup(destination);
    if(!string_is_empty(type)) {
        mount_data->type = strdup(type);
    }
    if(!string_is_empty(options)) {
        mount_data->options = strdup(options);
    }

    if(amxc_llist_append(&start_data->mounts, &mount_data->lit) < 0) {
        cthulhu_mount_data_delete(&mount_data);
        goto exit;
    }
    retval = 0;
exit:
    return retval;
}

int cthulhu_ctr_start_data_add_device_data(cthulhu_ctr_start_data_t* const start_data,
                                           const char* const device,
                                           const uint32_t major,
                                           const uint32_t minor,
                                           const char type,
                                           bool create) {
    int retval = -1;
    cthulhu_device_data_t* device_data = NULL;

    when_null(start_data, exit);
    when_null(device, exit);

    when_failed(cthulhu_device_data_new(&device_data), exit);
    device_data->device = strdup(device);
    device_data->major = major;
    device_data->minor = minor;
    device_data->type = type;
    device_data->create = create;

    if(amxc_llist_append(&start_data->devices, &device_data->lit) < 0) {
        cthulhu_device_data_delete(&device_data);
        goto exit;
    }
    retval = 0;
exit:
    return retval;
}

/*****************************/
/* EnvironmentVariables data */
/*****************************/

/**
 * @brief        cthulhu_envvar_data_new
 *
 * @detail       Assign memory to the structure cthulhu_envvar_data_t
 * @param[in]    envvar_data, contains key and value param in structure
 * @retval       0, Success
 *               -1, Failure
 */
int cthulhu_envvar_data_new(cthulhu_envvar_data_t** envvar_data) {
    int retval = -1;
    when_null(envvar_data, exit);

    *envvar_data = (cthulhu_envvar_data_t*) calloc(1, sizeof(cthulhu_envvar_data_t));
    when_null(*envvar_data, exit);

    if(cthulhu_envvar_data_init(*envvar_data) != 0) {
        cthulhu_envvar_data_delete(envvar_data);
        goto exit;
    }
    retval = 0;
exit:
    return retval;
}

/**
 * @brief        cthulhu_envvar_data_delete
 *
 * @detail       Call cleanup and free structure
 * @param[in]    envvar_data, contains key and value param in structure
 * @retval       void
 */
void cthulhu_envvar_data_delete(cthulhu_envvar_data_t** envvar_data) {
    when_null(envvar_data, exit);

    cthulhu_envvar_data_clean(*envvar_data);
    free_null(*envvar_data);
exit:
    return;
}

/**
 * @brief        cthulhu_envvar_data_init
 *
 * @detail       Assign NULL to cthulhu_envvar_data_t* structure variables
 * @param[in]    envvar_data, contains key and value param in structure
 * @retval       0, Success
 *               1, Failure
 */

int cthulhu_envvar_data_init(cthulhu_envvar_data_t* const envvar_data) {
    int retval = -1;
    when_null(envvar_data, exit);
    when_failed(amxc_llist_it_init(&envvar_data->lit), exit);
    envvar_data->key = NULL;
    envvar_data->value = NULL;

    retval = 0;
exit:
    return retval;
}

/**
 * @brief        cthulhu_envvar_data_clean
 *
 * @detail       Free parameter for cthulhu_envvar_data_t
 * @param[in]    envvar_data, contains key and value param in structure
 * @reval        void
 */
void cthulhu_envvar_data_clean(cthulhu_envvar_data_t* const envvar_data) {
    when_null(envvar_data, exit);

    free_null(envvar_data->key);
    free_null(envvar_data->value);
exit:
    return;
}

/**
 * @brief        cthulhu_envvar_data_reset
 *
 * @detail       Reset parameter of cthulhu_envvar_data_t*
 * @param[in]    envvar_data, contains key and value param in structure
 */
void cthulhu_envvar_data_reset(cthulhu_envvar_data_t* const envvar_data) {
    cthulhu_envvar_data_clean(envvar_data);
}

/**
 * @brief        cthulhu_envvar_data_it_free
 *
 * @detail       Iterates list and delete envvar data
 * @param[in]    it, iterates list amxc_llist_it_t
 */
static void cthulhu_envvar_data_it_free(amxc_llist_it_t* it) {
    cthulhu_envvar_data_t* envvar_data = amxc_llist_it_get_data(it, cthulhu_envvar_data_t, lit);
    cthulhu_envvar_data_delete(&envvar_data);
}

/**
 * @brief        cthulhu_ctr_start_data_add_envvar_data
 *
 * @detail       Populates container start data
 * @param[in]    start_data, Container start_data structure
 * @param[in]    key, Key for environment variable
 * @param[in]    value, Value for environment variable
 * @retval       0, Success
 *               -1, Failure
 */
int cthulhu_ctr_start_data_add_envvar_data(cthulhu_ctr_start_data_t* const start_data,
                                           const char* const key,
                                           const char* const value) {
    int retval = -1;
    cthulhu_envvar_data_t* envvar_data = NULL;

    when_null(start_data, exit);
    when_null(key, exit);
    when_null(value, exit);

    // populating envvar_data with key and value for each node
    when_failed(cthulhu_envvar_data_new(&envvar_data), exit);
    envvar_data->key = strdup(key);
    envvar_data->value = strdup(value);

    if(amxc_llist_append(&start_data->envvar, &envvar_data->lit) < 0) {
        cthulhu_envvar_data_delete(&envvar_data);
        goto exit;
    }
    retval = 0;
exit:
    return retval;
}

/*****************************/
/* usermapping data          */
/*****************************/

int cthulhu_usermapping_data_add_to_list(amxc_llist_t* usermappings,
                                         cthulhu_usermapping_type_t type,
                                         uid_t ctr_id,
                                         uid_t host_id,
                                         uint32_t range) {
    int res = -1;
    cthulhu_usermapping_data_t* usermapping_data = NULL;
    ASSERT_NOT_NULL(usermappings, goto exit);
    if((type <= cthulhu_usermapping_none) ||
       (type >= cthulhu_usermapping_last)) {
        SAH_TRACE_ERROR("Invalid type (%d)", type);
        goto exit;
    }
    ASSERT_SUCCESS(cthulhu_usermapping_data_new(&usermapping_data), goto exit);
    usermapping_data->type = type;
    usermapping_data->ctr_id = ctr_id;
    usermapping_data->host_id = host_id;
    usermapping_data->range = range;
    if(amxc_llist_append(usermappings, &usermapping_data->lit) < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not add usermapping to list");
        cthulhu_usermapping_data_delete(&usermapping_data);
        goto exit;
    }
    res = 0;
exit:
    return res;
}

int cthulhu_ctr_start_data_add_usermapping_data(cthulhu_ctr_start_data_t* const start_data,
                                                cthulhu_usermapping_type_t type,
                                                uid_t ctr_id,
                                                uid_t host_id,
                                                uint32_t range) {
    int retval = -1;
    when_null(start_data, exit);
    ASSERT_SUCCESS(cthulhu_usermapping_data_add_to_list(&start_data->usermappings,
                                                        type, ctr_id, host_id, range), goto exit);

    retval = 0;
exit:
    return retval;
}
