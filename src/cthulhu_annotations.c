/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "cthulhu_priv.h"
#include "cthulhu_assert.h"
#include <yajl/yajl_gen.h>
#include <amxc/amxc_variant_type.h>
#include <amxj/amxj_variant.h>
#include <libocispec/image_spec_schema_image_manifest_schema.h>

#include <cthulhu/cthulhu_defines.h>
#include <debug/sahtrace.h>

#include <cthulhu/cthulhu_annotations.h>

#define ME "annotations"

static const char*
cthulhu_oci_get_annotation_from_image_manifest(const image_spec_schema_image_manifest_schema* const manifest_schema,
                                               const char* annotation) {
    const char* last_annotation_value = NULL;
    when_null(manifest_schema, exit);
    when_null(annotation, exit);
    size_t it_annotations;

    if(manifest_schema->annotations) {
        for(it_annotations = 0; it_annotations < manifest_schema->annotations->len; it_annotations++) {
            char* annotation_key = manifest_schema->annotations->keys[it_annotations];
            if(strcmp(annotation, annotation_key) == 0) {
                last_annotation_value = manifest_schema->annotations->values[it_annotations];
            }
        }
    }
exit:
    return last_annotation_value;
}

static int cthulhu_json_to_var(const char* json_string,
                               amxc_var_t* parsed_var) {
    int ret = -1;
    amxc_var_t json_var;

    if(string_is_empty(json_string)) {
        SAH_TRACEZ_ERROR(ME, "json string is empty, so cannot parse");
        goto exit;
    }

    if(amxc_var_init(&json_var) < 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to init json_var");
        goto exit;
    }

    if(amxc_var_set(jstring_t, &json_var, json_string) < 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to set json_var");
        goto exit;
    }

    if(amxc_var_convert(parsed_var, &json_var, AMXC_VAR_ID_HTABLE) < 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to convert string to json\n%s", json_string);
        goto exit;

    }
    ret = 0;
exit:
    amxc_var_clean(&json_var);
    return ret;
}

void cthulhu_annotations_add_function(const char* const ctr_id,
                                      const image_spec_schema_image_manifest_schema* const image_manifest,
                                      const char* const annotation_key,
                                      cthulhu_annotation_fn fn) {
    const char* annotation_data = NULL;
    annotation_data = cthulhu_oci_get_annotation_from_image_manifest(image_manifest, annotation_key);
    if(annotation_data) {
        amxc_var_t parsed_var;
        SAH_TRACEZ_INFO(ME, "CTR[%s]: Contains %s", ctr_id, annotation_key);
        amxc_var_init(&parsed_var);
        cthulhu_json_to_var(annotation_data, &parsed_var);
        // Create user
        fn(ctr_id, &parsed_var);
        amxc_var_clean(&parsed_var);
    } else {
        SAH_TRACEZ_WARNING(ME, "CTR[%s]: Does not contain %s", ctr_id, annotation_key);
    }
}