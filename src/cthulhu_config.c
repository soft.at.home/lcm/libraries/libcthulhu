/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>

#include <amxc/amxc_string.h>
#include <amxc/amxc_llist.h>
#include <amxc/amxc_htable.h>
#include <amxc/amxc_variant.h>

#include <cthulhu/cthulhu_config.h>

#include "cthulhu_assert.h"
#include "cthulhu_priv.h"

int cthulhu_config_new(cthulhu_config_t** cfg) {
    int retval = -1;
    when_null(cfg, exit);

    *cfg = (cthulhu_config_t*) calloc(1, sizeof(cthulhu_config_t));
    when_null(*cfg, exit);

    if(cthulhu_config_init(*cfg) != 0) {
        cthulhu_config_delete(cfg);
        goto exit;
    }
    retval = 0;
exit:
    return retval;
}
void cthulhu_config_delete(cthulhu_config_t** cfg) {
    when_null(cfg, exit);

    cthulhu_config_clean(*cfg);

    free_null(*cfg);

exit:
    return;
}

int cthulhu_config_init(cthulhu_config_t* const cfg) {
    int retval = -1;
    when_null(cfg, exit);
    cfg->id = NULL;
    cfg->rootfs = NULL;
    cfg->bundle_name = NULL;
    cfg->bundle_version = NULL;
    cfg->sandbox = NULL;
    cfg->cgroup_dir = NULL;
    cfg->user = NULL;
    cfg->bundle_provided = false;
    when_failed(amxc_llist_init(&cfg->exposed_ports), exit);
    when_failed(amxc_llist_init(&cfg->env), exit);
    when_failed(amxc_llist_init(&cfg->entry_point), exit);
    when_failed(amxc_llist_init(&cfg->cmd), exit);
    when_failed(amxc_llist_init(&cfg->volumes), exit);
    cfg->working_dir = NULL;
    cfg->created = NULL;
    cfg->author = NULL;
    cfg->os = NULL;
    cfg->architecture = NULL;
    when_failed(amxc_htable_init(&cfg->labels, 0), exit);
    cfg->stop_signal = NULL;

    retval = 0;

exit:
    return retval;
}
void cthulhu_config_clean(cthulhu_config_t* const cfg) {
    when_null(cfg, exit);

    free_null(cfg->id);
    free_null(cfg->rootfs);
    free_null(cfg->bundle_name);
    free_null(cfg->bundle_version);
    free_null(cfg->sandbox);
    free_null(cfg->cgroup_dir);
    free_null(cfg->user);
    cfg->bundle_provided = false;
    amxc_llist_clean(&cfg->exposed_ports, variant_list_it_free);
    amxc_llist_clean(&cfg->env, variant_list_it_free);
    amxc_llist_clean(&cfg->entry_point, variant_list_it_free);
    amxc_llist_clean(&cfg->cmd, variant_list_it_free);
    amxc_llist_clean(&cfg->volumes, variant_list_it_free);
    free_null(cfg->working_dir);
    free_null(cfg->created);
    free_null(cfg->author);
    free_null(cfg->os);
    free_null(cfg->architecture);
    amxc_htable_clean(&cfg->labels, variant_htable_it_free);
    free_null(cfg->stop_signal);

exit:
    return;
}
void cthulhu_config_reset(cthulhu_config_t* const cfg) {
    cthulhu_config_clean(cfg);
}

