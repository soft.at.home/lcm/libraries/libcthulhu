/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <sys/time.h>
#include <sys/resource.h>

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <inttypes.h>
#include <limits.h>
#include <unistd.h>
#include <fcntl.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <cthulhu/cthulhu.h>

#include "test_ctr_info.h"

#define UNUSED __attribute__((unused))

void test_cthulhu_ctr_info_new_delete(UNUSED void** state) {
    cthulhu_ctr_info_t* info1 = NULL;
    cthulhu_ctr_info_t info2;

    assert_int_equal(cthulhu_ctr_info_new(&info1), 0);
    assert_null(info1->ctr_id);
    assert_null(info1->bundle);
    assert_null(info1->bundle_version);
    assert_null(info1->sb_id);
    assert_null(info1->created);
    assert_null(info1->owner);
    assert_int_equal(info1->pid, -1);
    assert_int_equal(info1->status, cthulhu_ctr_status_unknown);

    cthulhu_ctr_info_itf_t* itf = NULL;
    assert_int_equal(cthulhu_ctr_info_itf_new(&itf), 0);
    itf->name = strdup("eth0");
    amxc_llist_add_string(&itf->ips, "192.168.1.1");
    amxc_llist_append(&info1->interfaces, &itf->it);

    assert_int_equal(cthulhu_ctr_info_init(&info2), 0);
    assert_null(info2.ctr_id);
    assert_null(info2.bundle);
    assert_null(info2.bundle_version);
    assert_null(info2.sb_id);
    assert_null(info2.created);
    assert_null(info2.owner);
    assert_int_equal(info2.pid, -1);
    assert_int_equal(info2.status, cthulhu_ctr_status_unknown);

    info1->ctr_id = strdup("TEXT");
    info2.ctr_id = strdup("TEXT");

    assert_int_not_equal(cthulhu_ctr_info_new(NULL), 0);
    assert_int_not_equal(cthulhu_ctr_info_init(NULL), 0);

    cthulhu_ctr_info_reset(info1);
    cthulhu_ctr_info_reset(&info2);
    assert_null(info1->ctr_id);
    assert_null(info2.ctr_id);

    cthulhu_ctr_info_reset(NULL);

    cthulhu_ctr_info_delete(&info1);
    assert_null(info1);

    cthulhu_ctr_info_delete(NULL);
    cthulhu_ctr_info_clean(NULL);
}

void test_cthulhu_ctr_status_to_string_bounds(UNUSED void** state) {
    assert_string_equal(cthulhu_ctr_status_to_string(cthulhu_ctr_status_unknown), "Unknown");
    assert_string_equal(cthulhu_ctr_status_to_string(cthulhu_ctr_status_thawed), "Thawed");
    assert_ptr_equal(cthulhu_ctr_status_to_string(cthulhu_ctr_status_last), NULL);
    assert_ptr_equal(cthulhu_ctr_status_to_string(cthulhu_ctr_status_thawed + 1), NULL);
    assert_ptr_equal(cthulhu_ctr_status_to_string(-1), NULL);
    assert_ptr_equal(cthulhu_ctr_status_to_string(999), NULL);
}

void test_cthulhu_string_to_ctr_status_bounds(UNUSED void** state) {
    assert_int_equal(cthulhu_ctr_status_unknown, cthulhu_string_to_ctr_status("Unknown"));
    assert_int_equal(cthulhu_ctr_status_thawed, cthulhu_string_to_ctr_status("Thawed"));
    assert_int_equal(cthulhu_ctr_status_unknown, cthulhu_string_to_ctr_status(""));
    assert_int_equal(cthulhu_ctr_status_unknown, cthulhu_string_to_ctr_status("something"));
    assert_int_equal(cthulhu_ctr_status_unknown, cthulhu_string_to_ctr_status(NULL));
}

