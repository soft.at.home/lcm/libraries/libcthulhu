/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__CTHULHU_DEFINES_PLUGIN_H__)
#define __CTHULHU_DEFINES_PLUGIN_H__

#ifdef __cplusplus
extern "C"
{
#endif

#define CTHULHU_PLUGIN "CthulhuPlugin"

#define CTHULHU_PLUGIN_INIT "Init"
#define CTHULHU_PLUGIN_CLEANUP "Cleanup"

#define CTHULHU_PLUGIN_ODL_LOADED "OdlLoaded"

#define CTHULHU_PLUGIN_CTR_CREATE "CtrCreate"
#define CTHULHU_PLUGIN_CTR_VALIDATE "CtrValidate"
#define CTHULHU_PLUGIN_CTR_POSTCREATE "CtrPostCreate"
#define CTHULHU_PLUGIN_CTR_PRESTART "CtrPreStart"
#define CTHULHU_PLUGIN_CTR_POSTSTART "CtrPostStart"
#define CTHULHU_PLUGIN_CTR_STATECHANGED "CtrStateChanged"
#define CTHULHU_PLUGIN_CTR_PRESTOP "CtrPreStop"
#define CTHULHU_PLUGIN_CTR_POSTSTOP "CtrPostStop"
#define CTHULHU_PLUGIN_CTR_PREREMOVE "CtrPreRemove"
#define CTHULHU_PLUGIN_CTR_POSTREMOVE "CtrPostRemove"
#define CTHULHU_PLUGIN_CTR_MODIFY "CtrModify"

#define CTHULHU_PLUGIN_SB_CREATE "SbCreate"
#define CTHULHU_PLUGIN_SB_VALIDATE "SbValidate"
#define CTHULHU_PLUGIN_SB_MODIFY "SbModify"
#define CTHULHU_PLUGIN_SB_PRESTART "SbPreStart"
#define CTHULHU_PLUGIN_SB_POSTSTART "SbPostStart"
#define CTHULHU_PLUGIN_SB_STATECHANGED "SbStateChanged"
#define CTHULHU_PLUGIN_SB_PRESTOP "SbPreStop"
#define CTHULHU_PLUGIN_SB_POSTSTOP "SbPostStop"
#define CTHULHU_PLUGIN_SB_PREREMOVE "SbPreRemove"
#define CTHULHU_PLUGIN_SB_POSTREMOVE "SbPostRemove"

#define CTHULHU_PLUGIN_ARG_CTR_ID "ctr_id"
#define CTHULHU_PLUGIN_ARG_SB_ID "sb_id"
#define CTHULHU_PLUGIN_ARG_SB_HOST_DATA_DIR "sb_host_data_dir"
#define CTHULHU_PLUGIN_ARG_CREATE_DATA "data"
#define CTHULHU_PLUGIN_ARG_CTHULHU_CONFIG "cthulhuConfig"
#define CTHULHU_PLUGIN_ARG_PRESTART_CONFIG "preStartConfig"
#define CTHULHU_PLUGIN_ARG_BACKEND "backend"
#define CTHULHU_PLUGIN_ARG_DATAMODEL "datamodel"
#define CTHULHU_PLUGIN_ARG_IMAGE_MANIFEST "image_manifest"
#define CTHULHU_PLUGIN_ARG_ROOTFS "rootfs"
#define CTHULHU_PLUGIN_ARG_OLD_STATE "old_state"
#define CTHULHU_PLUGIN_ARG_NEW_STATE "new_state"
#define CTHULHU_PLUGIN_ARG_START_DATA "start_data"
#define CTHULHU_PLUGIN_ARG_MIB_GENERAL "mib_general"
#define CTHULHU_PLUGIN_ARG_MIB_CONTAINER "mib_container"
#define CTHULHU_PLUGIN_ARG_MIB_SANDBOX "mib_sandbox"
#define CTHULHU_PLUGIN_ARG_MIB_GENERAL_PRIVATE "mib_general_private"
#define CTHULHU_PLUGIN_ARG_MIB_CONTAINER_PRIVATE "mib_container_private"
#define CTHULHU_PLUGIN_ARG_MIB_SANDBOX_PRIVATE "mib_sandbox_private"
#define CTHULHU_PLUGIN_ARG_VALIDATE_ARG_KEY "validate_arg_key"
#define CTHULHU_PLUGIN_ARG_VALIDATE_ARG_VALUE "validate_arg_value"

#define CTHULHU_PLUGIN_RET_VALIDATE_ARG_VALID 0
#define CTHULHU_PLUGIN_RET_VALIDATE_ARG_INVALID 1
#define CTHULHU_PLUGIN_RET_VALIDATE_ARG_PASS 100

#ifdef __cplusplus
}
#endif

#endif // __CTHULHU_DEFINES_PLUGIN_H__
