/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__CTHULHU_CTR_START_DATA_H__)
#define __CTHULHU_CTR_START_DATA_H__

#ifdef __cplusplus
extern "C"
{
#endif


#ifndef __AMXC_STRING_H__
#error "include <amxc/amxc_string.h> before <cthulhu/cthulhu_config.h>"
#endif
#ifndef __AMXC_LLIST_H__
#error "include <amxc/amxc_llist.h> before <cthulhu/cthulhu_config.h>"
#endif
#ifndef __AMXC_HTABLE_H__
#error "include <amxc/amxc_htable.h> before <cthulhu/cthulhu_config.h>"
#endif

#include <cthulhu/cthulhu_sb_data.h>
#include <sys/types.h>

typedef struct _cthulhu_mount_data {
    amxc_llist_it_t lit; /**< list iterator */
    char* source;        /**< the file or dir on the host */
    char* destination;   /**< the file or dir in the container */
    char* type;          /**< the type of the filesystem to mount */
    char* options;       /**< fstab like options */
} cthulhu_mount_data_t;

int cthulhu_mount_data_new(cthulhu_mount_data_t** mount_data);
void cthulhu_mount_data_delete(cthulhu_mount_data_t** mount_data);
int cthulhu_mount_data_init(cthulhu_mount_data_t* const mount_data);
void cthulhu_mount_data_clean(cthulhu_mount_data_t* const mount_data);
void cthulhu_mount_data_reset(cthulhu_mount_data_t* const mount_data);

typedef struct _cthulhu_device_data {
    amxc_llist_it_t lit; /**< list iterator */
    char* device;        /**< the device in the container */
    uint32_t major;      /**< the major number of the device */
    uint32_t minor;      /**< the minor number of the device */
    char type;           /**< type: c for character or b for block */
    bool create;         /**< should the device be created */
} cthulhu_device_data_t;

int cthulhu_device_data_new(cthulhu_device_data_t** device_data);
void cthulhu_device_data_delete(cthulhu_device_data_t** device_data);
int cthulhu_device_data_init(cthulhu_device_data_t* const device_data);
void cthulhu_device_data_clean(cthulhu_device_data_t* const device_data);
void cthulhu_device_data_reset(cthulhu_device_data_t* const device_data);

typedef enum _cthulhu_usermapping_type {
    cthulhu_usermapping_none = 0,
    cthulhu_usermapping_user,
    cthulhu_usermapping_group,
    cthulhu_usermapping_last             // delimiter of enum
} cthulhu_usermapping_type_t;

typedef struct _cthulhu_usermapping_data {
    amxc_llist_it_t lit;             /**< list iterator */
    cthulhu_usermapping_type_t type; /**< the type of the mapping */
    uid_t ctr_id;                    /**< the uid or gid of the user in the container */
    uid_t host_id;                   /**< the starting uid or gid of the usermapping on the host */
    uint32_t range;                  /**< the number of uisers or groups that will be mapped */
} cthulhu_usermapping_data_t;

int cthulhu_usermapping_data_new(cthulhu_usermapping_data_t** usermapping_data);
void cthulhu_usermapping_data_delete(cthulhu_usermapping_data_t** usermapping_data);
int cthulhu_usermapping_data_init(cthulhu_usermapping_data_t* const usermapping_data);
void cthulhu_usermapping_data_clean(cthulhu_usermapping_data_t* const usermapping_data);
void cthulhu_usermapping_data_reset(cthulhu_usermapping_data_t* const usermapping_data);
void cthulhu_usermapping_data_it_free(amxc_llist_it_t* it);


int cthulhu_usermapping_data_add_to_list(amxc_llist_t* usermappings,
                                         cthulhu_usermapping_type_t type,
                                         uid_t ctr_id,
                                         uid_t host_id,
                                         uint32_t range);

typedef struct _cthulhu_ctr_start_data {
    cthulhu_sb_data_t* sb_data;
    char* syslogng;
    uint32_t run_user_id;
    uint32_t run_group_id;
    char* host_data_dir;
    // capabilities that will be kept in the container
    // lxc syntax:
    // - space separated capabilities, as defined in linux (e.g. CAP_NET_ADMIN)
    // - special keywords: 'all' and 'none'
    // if empty, nothing will be set
    char* capabilities;
    amxc_llist_t mounts;
    amxc_llist_t devices;
    amxc_llist_t envvar;
    amxc_llist_t usermappings;
} cthulhu_ctr_start_data_t;

int cthulhu_ctr_start_data_new(cthulhu_ctr_start_data_t** start_data);
void cthulhu_ctr_start_data_delete(cthulhu_ctr_start_data_t** start_data);

int cthulhu_ctr_start_data_init(cthulhu_ctr_start_data_t* const start_data);
void cthulhu_ctr_start_data_clean(cthulhu_ctr_start_data_t* const start_data);
void cthulhu_ctr_start_data_reset(cthulhu_ctr_start_data_t* const start_data);

int cthulhu_ctr_start_data_add_mount_data(cthulhu_ctr_start_data_t* const start_data,
                                          const char* const source,
                                          const char* const destination,
                                          const char* const type,
                                          const char* const options);

int cthulhu_ctr_start_data_add_device_data(cthulhu_ctr_start_data_t* const start_data,
                                           const char* const device,
                                           const uint32_t major,
                                           const uint32_t minor,
                                           const char type,
                                           bool create);

int cthulhu_ctr_start_data_add_usermapping_data(cthulhu_ctr_start_data_t* const start_data,
                                                cthulhu_usermapping_type_t type,
                                                uid_t ctr_id,
                                                uid_t host_id,
                                                uint32_t range);

typedef struct _cthulhu_envvar_data {
    amxc_llist_it_t lit; // list iterator
    char* key;           // Key for environment variable
    char* value;         // Value for environment variable
} cthulhu_envvar_data_t;

int cthulhu_envvar_data_new(cthulhu_envvar_data_t** envvar_data);
void cthulhu_envvar_data_delete(cthulhu_envvar_data_t** envvar_data);
int cthulhu_envvar_data_init(cthulhu_envvar_data_t* const envvar_data);
void cthulhu_envvar_data_clean(cthulhu_envvar_data_t* const envvar_data);
void cthulhu_envvar_data_reset(cthulhu_envvar_data_t* const envvar_data);

int cthulhu_ctr_start_data_add_envvar_data(cthulhu_ctr_start_data_t* const start_data,
                                           const char* const key,
                                           const char* const value);

#ifdef __cplusplus
}
#endif

#endif // __CTHULHU_CTR_START_DATA_H__


