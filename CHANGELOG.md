# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.29.0 - 2024-10-18(18:07:11 +0000)

### Other

- make default mount options of HostObjects configurable

## Release v0.28.0 - 2024-10-02(07:58:05 +0000)

### Other

- Implement unprivileged containers
- Add option to keep overlayfs mounted when stopped
- Cthulhu API validation
- modify tracezones

## Release v0.27.0 - 2024-07-17(10:51:58 +0000)

### Other

- Create USP Controller instance when a container added

## Release v0.26.5 - 2024-06-14(14:16:03 +0000)

## Release v0.26.4 - 2024-05-16(14:20:56 +0000)

### Other

- Rework onboarding as plugin

## Release v0.26.3 - 2024-04-30(14:41:30 +0000)

### Other

- Add container execute command capability

## Release v0.26.2 - 2024-03-19(13:45:19 +0000)

### New

- add defines for graceful shutdown timeout

## Release v0.26.1 - 2023-12-21(14:00:25 +0000)

### Other

- add defines for backend notification

## Release v0.26.0 - 2023-12-15(12:27:53 +0000)

### Other

- Add missing dependency on libsahtrace
- - Update license
- Service discovery - Advertisement via DNS.SD

## Release v0.25.1 - 2023-10-04(09:11:28 +0000)

### Other

- Opensource component

## Release v0.25.0 - 2023-09-21(09:09:55 +0000)

### Other

- Implement ModuleVersion parameter

## Release v0.24.0 - 2023-09-12(11:45:46 +0000)

### Other

- EE ModifyConstraints not aligned with standard

## Release v0.23.2 - 2023-09-07(10:18:00 +0000)

### Other

- Make component available on gitlab.softathome.com

## Release v0.23.1 - 2023-07-12(12:58:40 +0000)

### Other

- Add global networkconfig to timingila

## Release v0.23.0 - 2023-07-12(12:11:07 +0000)

### Other

- LCM containers onboarding solution - runtime

## Release v0.22.2 - 2023-07-03(13:03:40 +0000)

## Release v0.22.1 - 2023-06-29(09:38:36 +0000)

### Other

- Modify NetworkConfig

## Release v0.22.0 - 2023-06-15(11:27:45 +0000)

### Other

- NetworkConfig of containers

## Release v0.21.1 - 2023-06-02(11:05:59 +0000)

### Other

- DU/EU Unique Identifier

## Release v0.21.0 - 2023-05-31(15:13:20 +0000)

### Other

- remove deprecated dhcp settings

## Release v0.20.0 - 2023-05-16(13:10:12 +0000)

### Other

- add portforwarding to containers

## Release v0.19.0 - 2023-05-09(13:27:00 +0000)

### Other

- Add IP addresses to cthulhu_ctr_info

## Release v0.18.0 - 2023-04-27(12:08:06 +0000)

### Other

- HostObject support for devices sharing

## Release v0.17.0 - 2023-03-30(09:06:52 +0000)

### Other

- Implement HostObjects

## Release v0.16.0 - 2023-03-20(12:46:44 +0000)

### Other

- Add defines for backend init

## Release v0.15.3 - 2023-03-02(10:52:31 +0000)

### Other

- [libcthulhu] Use STAGING_LIBDIR instead of LIBDIR in src/makefile

## Release v0.15.2 - 2023-03-01(09:11:42 +0000)

### Other

- [LCM] Expose time for firstInstall and lastupdate

## Release v0.15.1 - 2023-02-07(13:37:10 +0000)

### Other

- [Timingila-rlyeh] Missing information on reboot

## Release v0.15.0 - 2023-01-30(15:42:49 +0000)

### Other

- update error codes with USP spec

## Release v0.14.10 - 2022-12-23(12:55:44 +0000)

### Other

- improve plugin functionality

## Release v0.14.9 - 2022-12-20(16:09:56 +0000)

### Other

- allow mounting different types

## Release v0.14.8 - 2022-11-23(12:17:24 +0000)

### Other

- Cthulhu should send error messages when it fails

## Release v0.14.7 - 2022-10-21(14:41:52 +0000)

### Other

- allow for several subdirs on repo

## Release v0.14.6 - 2022-09-19(07:18:07 +0000)

## Release v0.14.5 - 2022-09-09(11:19:30 +0000)

## Release v0.14.4 - 2022-08-29(13:06:53 +0000)

## Release v0.14.3 - 2022-07-06(15:29:23 +0000)

### Other

- if no network NS is defined, then the parent NS should be inheritted

## Release v0.14.2 - 2022-07-01(18:24:29 +0000)

### Other

- Parse autostart annotations

## Release v0.14.1 - 2022-06-28(12:57:33 +0000)

### Other

- Remove unused ParentSandbox network type

## Release v0.14.0 - 2022-06-03(15:33:18 +0000)

### New

- Add cthulhu_issocket function

### Other

- let syslogng in a container work when restarting the container

## Release v0.13.1 - 2022-05-25(14:57:24 +0000)

### Other

- - Parse resource restrictions from OCI image file

## Release v0.13.0 - 2022-05-23(15:16:44 +0000)

### New

- Parse mount annotations from image manifest

## Release v0.12.0 - 2022-05-18(21:42:05 +0000)

### New

- enable mounting of files and dirs

### Other

- enable mounting of files and dirs

## Release v0.11.0 - 2022-05-17(09:31:59 +0000)

### New

- Issue LCM-136 : Execution Environments: Add

## Release v0.10.0 - 2022-05-02(12:32:56 +0000)

### New

- Execution Environments: Modify. Description

## Release v0.9.6 - 2022-05-02(10:05:49 +0000)

### Other

- Logs: Machine Parsable. Execution Unit

## Release v0.9.5 - 2022-04-21(10:10:30 +0000)

### Fixes

- Remove sahtrace again since not compatible with opensource

## Release v0.9.4 - 2022-04-21(09:47:21 +0000)

### Fixes

- Issue: Dummy-000 exit forked process if execl fails

## Release v0.9.3 - 2022-04-09(11:49:41 +0000)

### Fixes

- Add missing defines
- add missing defines

## Release v0.9.2 - 2022-03-24(14:15:24 +0000)

### Other

- -  Add stats and resources to a container

## Release v0.9.1 - 2022-03-07(16:04:28 +0000)

### Other

- Command to modify resource constraints of an Execution Environment

## Release v0.9.0 - 2022-03-02(16:50:27 +0000)

### New

- Add define CTHULHU_CMD_SB_LS

## Release v0.8.0 - 2022-02-23(17:45:43 +0000)

### New

- Add Created timestamp to Sandbox

## Release v0.7.4 - 2022-02-22(08:26:10 +0000)

### Fixes

- rename AvailableDiskspace to AvailableDiskSpace
- close filehandle leak

## Release v0.7.3 - 2022-02-18(08:40:38 +0000)

### Other

- [LCM] Create user roles

## Release v0.7.2 - 2022-02-16(16:46:29 +0000)

### Other

- retrieve the available mem, diskspace of an EE (sandbox)

## Release v0.7.1 - 2022-02-15(10:23:39 +0000)

### Fixes

- disable format-nonliteral warning for openwrt builds

## Release v0.7.0 - 2022-02-10(20:29:51 +0000)

### New

- User overlayfs for containers

## Release v0.6.2 - 2022-01-26(15:37:10 +0000)

### Other

- ExecutionUnit: Restart

## Release v0.6.1 - 2022-01-21(09:48:10 +0000)

### Other

- Execution Environments: Restart

## Release v0.6.0 - 2021-12-23(16:18:13 +0000)

### New

- add tr181 error messages

## Release v0.5.0 - 2021-12-13(15:22:36 +0000)

### New

- add descriptor and vendor to DM

## Release v0.4.0 - 2021-12-13(08:19:38 +0000)

### New

- -  add Enable and restart() to Sandboxes

### Fixes

- IOT-000: update sandbox events

## Release v0.3.1 - 2021-12-03(01:51:55 +0000)

## Release v0.3.0 - 2021-11-26(13:46:54 +0000)

### New

- Implement updating of a container

## Release v0.2.4 - 2021-11-18(21:28:41 +0000)

### Other

- Create hierarchical sandboxes

## Release v0.2.3 - 2021-10-27(20:31:32 +0000)

### Other

- Implement Network NS support

## Release v0.2.2 - 2021-10-19(12:24:45 +0000)

### Other

- support UUID that start with a number

## Release v0.2.1 - 2021-10-06(20:00:36 +0000)

## Release v0.2.0 - 2021-10-06(18:29:35 +0000)

### New

- support removing of containers

## Release v0.1.1 - 2021-09-14(15:01:40 +0000)

### Fixes

- Do not fail on missing include dirs

## Release v0.1.0 - 2021-08-16(11:08:39 +0000)

### New

- Add support for execution environment

### Other

- Enbale auto opensourcing

## Release v0.0.8 - 2021-06-29(08:29:40 +0000)

- Enable g++ crosscompilation

## Release v0.0.7 - 2021-05-14(13:20:52 +0000)

### Changes

-  prepare repository for opensource

### Other

- add helper function

## Release v0.0.6 - 2021-04-26(13:15:31 +0000)

## Release 0.0.4 - 2020-12-18(10:35:28 +0100)

### Fixed
- Set lib path correctly

## Release 0.0.3 - 2020-12-16(20:44:06 +0000)

### Fixed
- Added missing bracket in cthulhu.h

## Release 0.0.2 - 2020-12-16(08:17:19 +0000)

### New

- initial release
